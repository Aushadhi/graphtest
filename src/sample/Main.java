package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.logging.Handler;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        init(primaryStage);
      //  Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        ////////primaryStage.setTitle("Hello World");
        //primaryStage.setScene(new Scene(root, 300, 275));
        //primaryStage.show();
    }
    private void init(Stage primaryStage){
        HBox root = new HBox();
        Scene scene = new Scene(root,450,330);

        CategoryAxis xAxis =new CategoryAxis();
        xAxis.setLabel("Year");
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Stock Value");

        LineChart<String, Number>lineChart = new LineChart<String, Number>(xAxis,yAxis);
        lineChart.setTitle("Stock Variation");

        XYChart.Series<String,Number>data =new XYChart.Series<>();
        data.getData().add(new XYChart.Data<String, Number>("JANUARY",18000));
        data.getData().add(new XYChart.Data<String, Number>("FEBRUARY",19020));
        data.getData().add(new XYChart.Data<String, Number>("MARCH",20230));
        data.getData().add(new XYChart.Data<String, Number>("APRIL",58000));
        data.getData().add(new XYChart.Data<String, Number>("MAY",25000));
        data.getData().add(new XYChart.Data<String, Number>("JUNE",26009));

        XYChart.Series<String,Number>data1 =new XYChart.Series<>();
        data1.getData().add(new XYChart.Data<String, Number>("JANUARY",18000));
        data1.getData().add(new XYChart.Data<String, Number>("FEBRUARY",23020));
        data1.getData().add(new XYChart.Data<String, Number>("MARCH",120230));
        data1.getData().add(new XYChart.Data<String, Number>("APRIL",28000));
        data1.getData().add(new XYChart.Data<String, Number>("MAY",55000));
        data1.getData().add(new XYChart.Data<String, Number>("JUNE",126009));

//
        lineChart.getData().add(data);
        lineChart.getData().add(data1);

        primaryStage.setTitle("Stock market sumulation");
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
